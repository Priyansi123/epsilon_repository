#!/bin/bash
#+-----------------------------------------------+
#|Purpose: Research and Development              |
#|Version: Ubuntu Linux 4.4.0-1092-aws           |
#|Created: 01 January 2020 06:05:36 am UTC       |
#| Author: Biswajit Ghosh                        |
#|   Type: Shell Script                          |
#|   User: ubuntu                                |
#+-----------------------------------------------+
#<:Script Start Here:>
#Conversion of UTC Time to IST Time
date -d '+5 hour 30 minute' '+%d %B %Y %I:%M:%S %P IST'

#<:Script  End  Here:>
